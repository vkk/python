import pprint, time

Composites = []; Primes = [2] 
Numbers = [i for i in range(2, 100000)]
start = time.clock()
for p in Primes:

    pcount = len(Primes)
    for n in Numbers:

        ''' check and mark values not primes; prevent duplicates
            (1) returns 0 if divided by current prime
            (2) does not equal current prime
            (3) is listed with values marked not prime '''
        if n % p == 0 and n != p and n not in Composites: 
            Composites.append(n)
            Numbers.remove(n) 

    ''' free the list of check-values after primes calculated '''
    if len(Numbers) == 1:
        Numbers = []

    ''' select next largest prime (p) from check-values list
        remove previous prime from check-values list
        append selected prime to primes list '''
    if len(Numbers) > 1: 
        p = Numbers[1]
        del Numbers[0]
        Primes.append(p)

    ''' output new primes-count when it updates '''
    if len(Primes) > pcount: 
        pcount = len(Primes); ncount = pcount + len(Composites)
        pprint.pprint('  %d primes of %d numbers ' % (pcount, ncount)) 

''' calculate program execution time '''
stop =  time.clock()
runtime = stop - start

''' populate two sublists with:
    (1) the first 100 primes, and 
    (2) the last 100 primes '''
first = Primes[:100]
last = Primes[9492:]
pp = pprint.PrettyPrinter(width = 4)

''' final output print block ''' 
pprint.pprint('-------------------------------------------------')
pprint.pprint(' Primes     %d, number of primes found ' % pcount)
pprint.pprint(' Numbers    %d, number of values checked ' % ncount)
pprint.pprint(' Composites %d, number of composites marked ' % len(Composites))
pprint.pprint(' Total runtime = %f seconds ' % runtime)
pprint.pprint('-------------------------------------------------') 
pp.pprint('First %d primes: %s' %(len(first), first)) 
pp.pprint('Last %d primes: %s' %(len(last), last))
pprint.pprint('-------------------------------------------------') 
