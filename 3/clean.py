#!/usr/bin/python
import os
import sys
sys.path.append("includes")
import stack, stringfind

class Cleaner():

    def __init__(self):
        self.errorFlag = False
        self.stk = stack.stack()
        self.dir = os.getcwd()

        self.ndirs = 0
        self.nfiles = 0
        self.pyc = []

        self.clean()

    def __repr__(self):
        if self.errorFlag: return "No directory or '.pyc' files found!"
        else: 
            print "\nCleaned directory of {} files".format(len(self.pyc))
	    print "The removed files with '.pyc' extension:"
            for x in self.pyc: 
                 print ' {}. {}'.format(self.pyc.index(x) + 1, x)
            return "\nTiffany Verkaik\t\t '3/tests/clean.py'\t\t 2015 Summer"

    def clean(self):
        self.stk.push(self.dir)
        while not self.stk.empty():
            path = self.stk.top()
            self.stk.pop()
            self.ndirs += 1
            files = os.listdir(path)
            for x in files:
                self.nfiles += 1
                fullpath = os.path.join(path, x)
                if os.path.isdir(fullpath): self.stk.push(fullpath)
                elif os.path.isfile(fullpath) and 
			stringfind.find(x, ".pyc") == True: 
                    self.pyc.append(x)
                    os.remove(fullpath)

if __name__ == "__main__":
    if len(sys.argv) != 1:
        print "Usage: ./clean.py"
        sys.exit()
    cleaner = Cleaner()
    print cleaner
