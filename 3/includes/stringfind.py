#!/usr/bin/python

def find(sentence, substring):
    try:
        L = sentence.split(' ')
        item_index = L.index(substring)
        M = [len(item) for item in L]
        char_index = item_index + sum(M[:item_index])
        if substring in L: return True 
    except Exception:
        N = list(sentence)
        O = list(substring)   
        sub_len = len(substring)
        first_ch = O[0]
        if first_ch in N and sub_len > 1:
            P = []
            pos = 0
            for ch in N:
                P.append(N[pos:pos + sub_len]) 
                pos += 1
                if O in P: return True 
        else: return False 

if __name__ == "__main__":
    str1 = raw_input('Type a string: ')
    str2 = raw_input('Type the substring to search for: ')
    result = find(str1, str2)
