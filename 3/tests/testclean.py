#!/usr/bin/python
import sys
import unittest
import generate
sys.path.append("..")
import clean

dirname = '' 
class TestClean(unittest.TestCase):

    def setUp(self):
        self.dir = dirname
        self.gpyc = []
        self.cpyc = []

    def tearDown(self):
        while len(self.gpyc) != 0:
            self.gpyc.pop()
        while len(self.cpyc) != 0:
            self.cpyc.pop()

    def testClean(self):
        self.assertTrue(len(self.gpyc) == 0) 
        generator = generate.Generator(self.dir)
        generator.generate()
        self.gpyc = [generator.pyc]
        
        self.assertTrue(len(self.cpyc) == 0)
        cleaner = clean.Cleaner()
        cleaner.clean()
        self.cpyc = [cleaner.pyc]

        self.assertEqual(len(self.gpyc), len(self.cpyc))
        print generator
        print cleaner

if __name__ == "__main__":
    if len(sys.argv) != 2:
        print "Usage: ./testclean.py <directory_name>"
        sys.exit()
    dirname = sys.argv[1]
    del sys.argv[1]
    unittest.main()
