#!/usr/bin/python

import os
import sys
import gzip
import random
import string
sys.path.append("../includes")
import stringfind

EXTENSIONS = ['o', '.pyc', '.txt', '.cpp', '.py', '.c']
MAX_MYSTERY_WORDS = 20

class Generator(object):
    def __init__(self, dirname):
        self.errorFlag = False
        self.pwd = os.getcwd() 
        self.dir = dirname

        self.pyc = []
        self.words = []
        self.files = []

        self.__ndirs = 0
        self.__nfiles = 0

        self.mysteryFile = ''
        self.mysteryWords = []

        try:
            dict = gzip.open('webster.txt.gz')
        except IOError:
            dict = gzip.open('tests/webster.txt.gz')
        except:
            print "Couldn't find webster!"
            sys.exit(-1)
        for line in dict: self.words.append(line.rstrip('\n'))

    def __repr__(self):
        if self.errorFlag: return "No directory or files created!"
        else: 
            print "\nMystery file name: {}".format(self.mysteryFile) 
            print "New directory is {}".format(self.dir) 
            print "Generated {} Subdirectories".format(str(self.__ndirs))  
            print "Generated {} Files\n".format(str(self.__nfiles)) 
	    print "The generated files with '.pyc' extention:"
            for x in self.pyc: 
                print ' {}. {}'.format(self.pyc.index(x) + 1, x)
            return "\nTiffany Verkaik\t\t '3/tests/generate.py'\t\t 2015 Summer"

    def getNDirs(self): return self.__ndirs
    def getNFiles(self): return self.__nfiles

    def writeFile(self, filename):
        if stringfind.find(filename, ".pyc") == True:
            self.pyc.append(filename)
        self.files.append(filename)
        FILE = open(filename,"w")
        self.__nfiles += 1
        n = random.randint(5,10)
        for x in range(1, n):
            w = random.randint(0, len(self.words))
            word = self.words[w]
            FILE.write(word)
            FILE.write(' ')
        FILE.write('\n')
        FILE.close()

    def writeMysteryFile(self):
        toDoThis = random.randint(0,2)
        if not toDoThis: return
        self.files.append(self.mysteryFile)
        FILE = open(self.mysteryFile,"w")
        self.__nfiles += 1
        for word in self.mysteryWords:
            FILE.write(word)
            FILE.write(' ')
        FILE.write('\n')
        FILE.close()

    def makeMysteryFilename(self):
        n = random.randint(3,7)
        w = random.randint(0, len(self.words))
        name = self.words[w]
        self.mysteryFile = name+'.mys'
        max = random.randint(10, MAX_MYSTERY_WORDS)+10
        for index in range(10, max):
            w = random.randint(0, len(self.words))
            word = self.words[w]
            self.mysteryWords.append(word)

    def makeFiles(self, extension):
        n = random.randint(3,7)
        for x in range(2,n):
            w = random.randint(0, len(self.words))
            name = self.words[w]
            newfile = name+extension
            self.writeFile(newfile)

    def generate(self):
        if os.path.isdir(self.dir) or os.path.isfile(self.dir):
            print str(self.dir), \
                "already exists, choose a non-existing directory."
            self.errorFlag = True
            return
        self.makeMysteryFilename()
        d  = self.dir
        for x in range(1,4):
            os.mkdir(d)
            self.__ndirs += 1
            os.chdir(d)
            if x == 3: self.writeMysteryFile()
            for extension in EXTENSIONS:
                self.makeFiles(extension)
            n = random.randint(0, len(self.words))
            d = self.words[n]
        os.chdir(self.pwd)

if __name__ == "__main__":
    if len(sys.argv) != 2:
        print "Usage: ./generate.py <directory_name>"
        sys.exit()
    generator = Generator(sys.argv[1])
    generator.generate()
    print generator 
