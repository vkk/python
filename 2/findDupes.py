# Tiffany Verkaik	CPSC 3720: Software Development
# 7 July 2015		Project Two - Scripts and Testing 

import os
import sys
import includes.walker as walker

class FindDupes(object):
    def __init__(self, dirname):
        self.dir = dirname
        self.noDirsGen = 0
        self.noFilesGen = 0
        self.singles = []
        self.dupes = []
        self.dupeMaps = []
        self.walker = walker.Walker(self.dir)

    def dupescheck(self):
        self.walker.walk()
        for x in self.walker.filemaps:
            if self.walker.files.count(x.keys()[0]) > 1:
                self.dupes.append(x.keys()[0])
                self.dupeMaps.append(x)
            elif x.keys()[0] in self.dupes:
                self.dupeMaps.append(x)
            else:
                self.singles.append(x)

    def dupesdisplay(self):
        print "\nfindDupes.py returned", len(self.dupeMaps) - 1, \
            "duplicate(s) of", self.dupeMaps[0].keys()[0]
        print "[1st occurrence]", self.dupeMaps[0].values()[0] 
        results = self.dupeMaps[1:]
        for x in results:
            print "\t\t>", self.dupeMaps[self.dupeMaps.index(x)].values()[0] 

if __name__ == "__main__":
    if len(sys.argv) != 2:
        print "Usage: python findDupes.py <directory>"
        sys.exit()
    elif os.path.isfile(sys.argv[1]):
        print sys.argv[1], "is a file - usage: python findDupes.py <directory>"
        sys.exit()
    else:
        dupes = FindDupes(sys.argv[1])
        dupes.dupescheck()
        dupes.dupesdisplay()
