class stack(object):
  def __init__(self):
    self.stk = []

  def empty(self):
    return len(self.stk) == 0

  def push(self, x):
    self.stk.append(x)

  def pop(self):
    self.stk.pop()

  def top(self):
    return self.stk[-1]

  def __repr__(self):
    s = '('
    for x in self.stk:
      s = s + str(x)+', '
    if len(s) > 1:
      s = s[0:-2]
    s += ')'
    return s

if __name__ == "__main__":
  s = stack()
  print s.empty()
  s.push(1)
  s.push(2)
  s.push(3)
  print s
  print s.top()
  s.pop()
  print s
