import os, sys
import stack

class Walker(object):
  def __init__(self, dir):
    self.start = dir
    self.dirs = []
    self.files = []
    self.filemaps = []
    self.stk = stack.stack()

  def walk(self):
    self.stk.push(self.start)
    while not self.stk.empty():
      path = self.stk.top()
      self.stk.pop()
      files = os.listdir(path)
      for x in files:
        fullpath = os.path.join(path, x)
        if os.path.isdir(fullpath):
          self.dirs.append(x)
          self.stk.push(fullpath)
        else:
          self.files.append(x)
          self.filemaps.append({x:fullpath})

  def display(self):
    print "Walked", self.start
    print "Subdirectories", self.dirs
    print "The", len(self.files), "files are:"
    print self.files

if __name__ == "__main__":
  if len(sys.argv) != 2:
    print "usage:", sys.argv[0], "<directory>"
    sys.exit()
  walker = Walker(sys.argv[1])
  walker.walk()
  walker.display()
