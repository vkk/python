import unittest
import sys
sys.path.append("../include")
import stack

class TestStack(unittest.TestCase):
  def setUp(self):
    self.stk = stack.stack()

  def tearDown(self):
    while not self.stk.empty():
      self.stk.pop()

  def testEmpty(self):
    self.assertEqual(self.stk.empty(), True)

  def testTop(self):
    self.assertEqual(self.stk.empty(), True)
    for i in range(3):
      self.stk.push(i)
    self.assertEqual(self.stk.top(), 2)
    count = 2
    while not self.stk.empty():
      self.assertEqual(self.stk.top(), count)
      self.stk.pop()
      count -= 1

  def testPush(self):
    self.assertEqual(self.stk.empty(), True)
    self.stk.push(99)
    self.assertFalse(self.stk.empty(), True)
    for i in range(3):
      self.stk.push(i)
    self.assertFalse(self.stk.empty(), True)
    for i in range(3):
      self.stk.pop()
    self.assertFalse(self.stk.empty(), True)
    self.stk.pop()
    self.assertEqual(self.stk.empty(), True)
  

if __name__ == "__main__":
  unittest.main()
