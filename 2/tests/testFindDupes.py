import os
import sys 
import unittest
import generate
sys.path.append("..")
import findDupes
import includes.walker as walk

class TestFindDupes(unittest.TestCase):

    dirname = None
    dupeMaps = []
    def setUp(self):
        self.dir = self.dirname
        self.noDirsGen = 0
        self.noFilesGen = 0
        self.singles = []
        self.dupes = []

    def testFindDupes(self):
        generator = generate.Generator(self.dir)
        generator.generate()
        self.noDirsGen = generator.getNoDirs()
        self.noFilesGen = generator.getNoFiles()
        walker = walk.Walker(self.dir)
        walker.walk()
 	for x in walker.filemaps: 
            if walker.files.count(x.keys()[0]) > 1:
                self.dupes.append(x.keys()[0])
                self.dupeMaps.append(x)
            elif x.keys()[0] in self.dupes:
                self.dupeMaps.append(x)
            else:
                self.singles.append(x)
        noDirs = len(walker.dirs)
        self.assertTrue(noDirs <= self.noDirsGen)
	noFiles = len(walker.files)
        self.assertEqual(noFiles, self.noFilesGen)
        print "\ntestFindDupes() returned", len(self.dupeMaps) - 1, \
	    "duplicate(s) of", self.dupeMaps[0].keys()[0]
        print "[1st occurrence]", self.dupeMaps[0].values()[0]
        results = self.dupeMaps[1:]
        for x in results:
            print "\t\t>", self.dupeMaps[self.dupeMaps.index(x)].values()[0]

    def testFindDupesFunction(self):
	findfunc = findDupes.FindDupes(self.dir)
        findfunc.dupescheck()
        findfunc.dupesdisplay()
	self.assertEqual(findfunc.noDirsGen, self.noDirsGen)
	self.assertEqual(findfunc.noFilesGen, self.noFilesGen)
	self.assertTrue(len(findfunc.dupeMaps) == len(self.dupeMaps))
	self.assertEqual(findfunc.dupeMaps, self.dupeMaps)
         
if __name__ == "__main__":
    if len(sys.argv) != 2:
        print "Usage: python testFindDupes.py <new-directory>"
        sys.exit()
    TestFindDupes.dirname = sys.argv[1]
    del sys.argv[1]
    unittest.main()
